const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/


const usersArray = Object.entries(users).filter(function(element){
    if(element[1].interests[0].indexOf("Video Games") != -1 || element[1].interests[0].indexOf("Video Games") != -1){
        return true;
    }else{
        return false;
    }
    
});


const usersInGermany = Object.entries(users).filter(function(element){
    return element[1].nationality == 'Germany';
});


const sortedInSeniority = Object.entries(users).sort(function(a, b){
    const aSeniority = a[1].desgination;
    const bSeniority = b[1].desgination;
    if(aSeniority.indexOf('Senior') != -1){
        return true;
    }else if(aSeniority.indexOf('Developer') != -1 && !bSeniority.indexOf("Senoir") == -1){
        return true;
    }else{
        return false;
    }
});


const usersWithMasters = Object.entries(users).filter(function(element){
    return element[1].qualification == 'Masters';
})


const groupedUsers = Object.entries(users).reduce(function(accumulator, currentValue){
    accumulator["Python"] = [];
    accumulator["Javascript"] = [];
    accumulator['Golang'] = [];
    if(currentValue[1].desgination.indexOf('Python') != -1){
        accumulator["Python"].push(currentValue[0]);
    }
    if(currentValue[1].desgination.indexOf('Javascript') != -1){
        accumulator["Javascript"].push(currentValue[0]);
    }
    if(currentValue[1].desgination.indexOf('Golang') != -1){
        accumulator['Golang'].push(currentValue[0]);
    }
    return accumulator;
}, {});
